import {
  Box,
  Button,
  Text,
  useDisclosure,
  useToast,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Spacer,
  Center,
} from "@chakra-ui/react";
import {
  Action,
  Asset,
  AssetExtended,
  Data,
  resolveDataHash,
  resolvePaymentKeyHash,
  Transaction,
  UTxO,
} from "@meshsdk/core";
import { useAddress, useWallet } from "@meshsdk/react";
import { useContext, useEffect, useState } from "react";
import { contributorPolicyID, escrow, issuerAsset, projectAsset, treasury, treasuryReferenceUTxO } from "@/gpte-config";
import { ProjectDatum, ProjectTxMetadata } from "@/src/types/project";
import { GraphQLToDatum, GraphQLToMeshUTxO, stringToHex } from "@/src/utils";
import { PPBLContext } from "@/src/context/PPBLContext";
import { ConnectWalletMessage } from "../../ui/Text/ConnectWalletMessage";

type Props = {
  newModuleList: string[];
};

const UpdateProjectList: React.FC<Props> = ({ newModuleList }) => {
  const { connected, wallet } = useWallet();
  const address = useAddress();

  const ppblContext = useContext(PPBLContext);

  const [walletHasCollateral, setWalletHasCollateral] = useState(false);
  const [connectedUtxos, setConnectedUtxos] = useState<UTxO[]>([]);

  const [treasuryContractUTxO, setTreasuryContractUTxO] = useState<UTxO | undefined>(undefined);
  const [treasuryContractDatum, setTreasuryContractDatum] = useState<string[] | undefined>(undefined);
  const [constructedTreasuryDatum, setConstructedTreasuryDatum] = useState<Data | undefined>(undefined);

  useEffect(() => {
    if (ppblContext.treasuryUTxO) {
      const _tUTxO = GraphQLToMeshUTxO(ppblContext.treasuryUTxO)
      setTreasuryContractUTxO(_tUTxO);
    }
  }, [ppblContext]);

  useEffect(() => {
    if (treasuryContractDatum) {
      const _constructedTreasuryDatum: Data = {
        alternative: 0,
        fields: [newModuleList, "5050424c3230323354656163686572"],
      };
      setConstructedTreasuryDatum(_constructedTreasuryDatum);
    }
  }, [treasuryContractDatum, newModuleList]);

  // UI Helpers:
  const [txLoading, setTxLoading] = useState(false);
  const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null);
  const [loadContrib, setLoadContrib] = useState(true);

  // Specific to each Project:
  const [currentTreasuryRedeemer, setCurrentTreasuryRedeemer] = useState<Partial<Action> | null>(null);
  const [constructedProjectDatum, setConstructedProjectDatum] = useState<Data | undefined>(undefined);

  // Transaction Building:
  const [utxoBackToTreasury, setUtxoBackToTreasury] = useState<Partial<UTxO> | undefined>(undefined);

  // For Chakra Modal:
  const { isOpen: isConfirmationOpen, onOpen: onConfirmationOpen, onClose: onConfirmationClose } = useDisclosure();
  const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure();

  // Check the connected wallet for a PPBL 2023 Contributor token
  // If there are many, return the first one in the list returned by getPolicyIdAssets
  useEffect(() => {
    const fetchContributorUtxo = async () => {
      const _utxos = await wallet.getUtxos();
      if (_utxos.length > 0) {
        setConnectedUtxos(_utxos);
      }
    };

    const fetchConnectedCollateral = async () => {
      const _collateral = await wallet.getCollateral();
      if (_collateral.length > 0) {
        setWalletHasCollateral(true);
      }
    };

    if (connected) {
      fetchContributorUtxo();
      fetchConnectedCollateral();
      setLoadContrib(true);
    }
  }, [connected, loadContrib]);

  // Set the data treasuryRedeemer to match constructedProjectDatum,
  // because in GPTE Contracts, the Treasury Redeemer and Project Datum consist of the same parameters.
  useEffect(() => {
    if (constructedProjectDatum) {
      const _treasuryRedeemer: Partial<Action> = {
        data: {
          alternative: 1,
          fields: [],
        },
      };
      setCurrentTreasuryRedeemer(_treasuryRedeemer);
    }
  }, [constructedProjectDatum]);

  useEffect(() => {
    if (ppblContext.connectedIssuerToken && ppblContext.treasuryUTxO && treasuryContractUTxO) {
      const assetsAtTreasury: Asset[] = treasuryContractUTxO.output.amount;

      // Calculate the number of Lovelace that will be sent back to Treasury
      const lovelaceAtTreasury = assetsAtTreasury.filter((asset) => asset.unit === "lovelace");

      const gimbalsAtTreasury = assetsAtTreasury.filter((asset) => asset.unit === projectAsset);

      // Create Asset[] for each output UTxO
      const _assetsBackToTreasury: Asset[] = [
        {
          unit: "lovelace",
          quantity: lovelaceAtTreasury[0].quantity,
        },
        {
          unit: projectAsset,
          quantity: gimbalsAtTreasury[0].quantity,
        },
      ];

      // Create the UTxOs
      const _utxoTreasury: Partial<UTxO> = {
        output: {
          address: treasury.address,
          amount: _assetsBackToTreasury,
        },
      };

      setUtxoBackToTreasury(_utxoTreasury);
    }
  }, [connected, ppblContext.connectedIssuerToken]);

  const handleCommitmentTx = async () => {
    if (ppblContext.connectedIssuerToken) {
      setTxLoading(true);
      try {
        const tx = new Transaction({ initiator: wallet })
          .redeemValue({
            value: treasuryContractUTxO,
            script: treasuryReferenceUTxO,
            datum: treasuryContractUTxO,
            redeemer: currentTreasuryRedeemer,
          })
          .sendValue(
            {
              address: treasury.address,
              datum: {
                value: constructedTreasuryDatum,
                inline: true,
              },
            },
            utxoBackToTreasury
          )
          .sendAssets(
            address,
            [{unit: issuerAsset, quantity: 1}],
          );
        console.log("So far so good.", tx);

        const unsignedTx = await tx.build();
        const signedTx = await wallet.signTx(unsignedTx, true);
        const txHash = await wallet.submitTx(signedTx);
        setSuccessfulTxHash(txHash);
        console.log("Success!", txHash);
        onSuccessOpen();
        onConfirmationClose();
      } catch (error: any) {
        if (error.info) {
          alert(error.info);
        } else {
          console.log(error);
        }
      }
      setTxLoading(false);
    }
  };

  return (
    <>
      <Center mx="auto">
        <Box border="1px" borderRadius="md" borderColor="theme.light" p="2">
          <Box bg="theme.green" color="theme.dark" p="1">
            <Text fontSize="xs">Update Treasury</Text>
          </Box>
          {connected && ppblContext.connectedIssuerToken ? (
            <>
              <Box bg="theme.green" color="theme.dark" p="1" mt="2">
                <Text fontSize="xs">Tx will lock Contributor Token:</Text>
              </Box>
              <Box>
                {JSON.stringify(treasuryContractUTxO)}
              </Box>
              <Box bg="theme.light" color="theme.dark" p="2">
                <Text fontSize="xl">{ppblContext.connectedIssuerToken}</Text>
              </Box>
              <Center w={["100%"]} mt="2">
                <Button colorScheme="orange" onClick={onConfirmationOpen} size="sm">
                  Update Module List
                </Button>
              </Center>
            </>
          ) : (
            <>
              {connected ? (
                <Box bg="theme.yellow" color="theme.dark" p="1" mt="2">
                  <Text fontSize="md">A Commitment Transaction requires a PPBL2023 Token</Text>
                </Box>
              ) : (
                <Box mt="2">
                  <ConnectWalletMessage />
                </Box>
              )}
            </>
          )}
        </Box>
      </Center>

      <Modal blockScrollOnMount={false} isOpen={isSuccessOpen} onClose={onSuccessClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Successful Tx!</ModalHeader>
          <ModalBody>
            <Text py="2">Transaction ID: {successfulTxHash}</Text>
            <Text py="2">It may take a few minutes for this tx to show up on a blockchain explorer.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="white" color="gray.700" onClick={onSuccessClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal blockScrollOnMount={false} isOpen={isConfirmationOpen} onClose={onConfirmationClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Update Module List</ModalHeader>
          {walletHasCollateral ? (
            <ModalBody>
              <Text py="2">The Treasury Datum will be updated</Text>
            </ModalBody>
          ) : (
            <ModalBody>
              <Text py="2">Please set Collateral in your wallet.</Text>
            </ModalBody>
          )}

          <ModalFooter>
            {walletHasCollateral && (
              <Button colorScheme="orange" onClick={handleCommitmentTx}>
                Update Module List
              </Button>
            )}
            <Spacer />
            <Button bg="white" color="gray.700" onClick={onConfirmationClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UpdateProjectList;
