import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module202.json";
import Docs2021 from "@/src/components/course-modules/202/Docs2021.mdx";

export default function Lesson2021() {
  const slug = "2021";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={202} sltId="202.1" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2021 />
    </LessonLayout>
  );
}
